module ApplicationHelper
  def current_user
    return unless session[:user_id]
    @current_user ||= User.find(session[:user_id])
  end

  def alert_for(flash_type)
    { success: 'alert-success',
      error: 'alert-danger',
      alert: 'alert-warning',
      notice: 'alert-info'
    }[flash_type.to_sym] || flash_type.to_s
  end

  def float_arr2string(n)
    n.map{ |a| a.to_s }.join(" ")
  end

  def string2float_arr(s)
    n.split(" ").map{ |a| a.to_f }
  end

  def highest_score(s)
    string2float_arr(s)[0]
  end
end
