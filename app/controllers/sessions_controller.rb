class SessionsController < ApplicationController
  def new
    redirect_to root_path unless session[:user_id].nil?
  end

  def create
    user = User.find_by_name(params[:session][:name])
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      flash.now[:success] = "Logged in successfully."
    else
      flash.now[:alert] = "Wrong username and/or password."
    end
    respond_to do |format|
      format.js
    end
  end

  def destroy
    session[:user_id] = nil
    flash.now[:success] = "Logged out successfully."
    respond_to do |format|
      format.js
    end
  end
end
