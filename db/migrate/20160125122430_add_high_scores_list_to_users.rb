class AddHighScoresListToUsers < ActiveRecord::Migration
  def change
    add_column :users, :high_scores, :string
  end
end
